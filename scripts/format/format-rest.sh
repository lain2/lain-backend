#!/usr/bin/env bash

echo "============REST==================="
poetry run autoflake --in-place --remove-unused-variables --remove-all-unused-imports app/rest/*/*.py
poetry run black app/rest/*
poetry run isort app/rest/
echo "==================================="
