#!/usr/bin/env bash

# poetry run mypy app/core/*
echo "============CORE==================="
poetry run autoflake --in-place --remove-unused-variables --remove-all-unused-imports app/data/*/*.py
poetry run black app/core/*
poetry run isort app/core/*/*.py
echo "==================================="

